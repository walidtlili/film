# import config.
cnf ?= .env
include $(cnf)
export $(shell sed 's/=.*//' $(cnf))

# DOCKER TASKS
# Build the container
build: ## Build the container
	docker-compose build

run: ## Run container on port configured in `.env`
	docker-compose up -d
	docker-compose exec -u 1000 $(PHP_CONTAINER_NAME) composer install


up: build run ## Run container on port configured in `.env` (Alias to run)

down: ## Stop and remove a running container
	docker-compose down -v --remove-orphans

