$(document).ready(function () {

    $('.js-film-autocomplete').each(function () {
        var autocompleteUrl = $(this).data('autocomplete-url');

        $(this).autocomplete({hint: true}, [
            {
                source: function (query, cb) {

                    var categories = $('#categories:checked').map(function(){
                        return this.value; }).get().join(",");
                    $.ajax({
                        url: autocompleteUrl + '?query=' + query +
                            '&categories=' + categories
                    }).then(function (data) {
                        cb(data);
                    });
                },
                displayKey: 'title',
                debounce: 500, // only request every 1/2 second
                change: function (event, ui) {
                    console.log(event);
                }
            }
        ])
    });
});