<?php

namespace App\Repository;

use App\Entity\Film;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Film|null find($id, $lockMode = null, $lockVersion = null)
 * @method Film|null findOneBy(array $criteria, array $orderBy = null)
 * @method Film[]    findAll()
 * @method Film[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Film::class);
    }

    // /**
    //  * @return Film[] Returns an array of Film objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Film
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param string $query
     * @param string $query
     * @param int $limit
     * @return Film[]
     */
    public function findAllMatching(string $query, string $categories, int $limit = 5)
    {
        $qb = $this->createQueryBuilder('f');
        if ($query !== '') {
            $qb->where('f.title LIKE :query')
                ->setParameter('query', '%' . $query . '%');
        }
        if ($categories !== '') {
            $qb->leftJoin('f.category', 'c')
                ->andWhere('c.title in (:categories)')
                ->setParameter('categories', $categories);
        }
        return $qb
            ->setMaxResults($limit)
            ->getQuery()->getArrayResult();
    }
}
