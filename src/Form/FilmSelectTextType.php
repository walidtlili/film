<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

class FilmSelectTextType extends AbstractType
{
    private RouterInterface $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'data-autocomplete-url' => $this->router->generate('film'),
                'class' => 'form-control js-film-autocomplete',
                'placeholder' => 'Search'
            ]
        ]);
    }

    public function getParent()
    {
        return TextType::class;
    }
}