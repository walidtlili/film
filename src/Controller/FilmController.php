<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Film;
use App\Form\FilmSelectTextType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FilmController extends AbstractController
{
    #[Route('/film', name: 'film')]
    public function index(Request $request): Response
    {
        $form = $this->createFormBuilder(new Film())
            ->add('title', FilmSelectTextType::class, ['label' => ' '])
            ->getForm();

        if($request->isXmlHttpRequest()){
            $films = $this->getDoctrine()->getManager()->getRepository(Film::class)
                ->findAllMatching($request->query->get('query'), $request->query->get('categories'));
            return new JsonResponse($films);
        }

        $films = $this->getDoctrine()->getManager()->getRepository(Film::class)->findAll();
        $categories = $this->getDoctrine()->getManager()->getRepository(Category::class)->findAll();

        return $this->render('film/index.html.twig', [
            'controller_name' => 'FilmController',
            'films' => $films,
            'categories' => $categories,
            'form' => $form->createView()
        ]);
    }
}
